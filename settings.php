<?php
// Specify EMAIL (FROM) Address:
$MAIL_SERVER_FROM = "silicaandpina@vitatricks.xyz";
// Specify Site URL to be shown in messages
$SITE_NAME = "http://vitatricks.xyz";
// Specify message to be sent with every email
$MAIL_BODY = 'This ICS file can be used to call applications on the PSVita.<br>to do this; Load this email in the PSVitas email client<br>and tap the WWW Browser icon.<br><br><br><i>This email was sent from '.$SITE_NAME.'</i>';
?>
